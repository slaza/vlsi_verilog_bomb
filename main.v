/*
	odbrojavanje od 9 do 1 nakon pritiska SW0
	nakon toga reaguje se samo na reset
	na isteku ostaje 0
*/

module main(
	input wire reset,
	input wire start,
	input wire clk,
	
	output wire [6:0] HEX0,
	output wire [6:0] HEX1,
	output wire [6:0] HEX2,
	output wire [6:0] HEX3,
	
	input wire sw1,
	input wire sw2,
	input wire sw3,
	
	input wire [2:0] but,
	output reg [3:0] led
);

	//konstante
	parameter SEKUNDA = 50_000_000;
	parameter POLA_SEKUNDE = 25_000_000;
	
	localparam INICIJALIZACIJA = 3'b000;
	localparam ODBROJAVANJE = 3'b001;
	localparam TREPTANJE = 3'b010;
	localparam ZAMRZNUTO = 3'b011;
	
	localparam BUTTON_0 = 2'b01;
	localparam BUTTON_1 = 2'b10;
	localparam BUTTON_2 = 2'b11;
	
	localparam TREPTI_DISP = 4'h8;
	localparam ISKLJUCEN_DISP = 4'h0;
	//stanja
	reg [2:0] state_reg, state_next;
	
	//sekundara
	integer timer_reg, timer_next;
	
	//ss displeji
	reg [3:0] active_disp_reg;
	reg [3:0] active_disp_next;
	
	reg [3:0] disp0_input;
	reg [3:0] disp1_input;
	reg [3:0] disp2_input;
	reg [3:0] disp3_input;
	//registri
	reg r0_load, r0_dec;
	wire [3:0] r0_output;
	reg [3:0] r0_input;

	reg r1_load, r1_dec;
	wire [3:0] r1_output;
	reg [3:0] r1_input;

	reg r2_load, r2_dec;
	wire [3:0] r2_output;
	reg [3:0] r2_input;

	reg r3_load, r3_dec;
	wire [3:0] r3_output;
	reg [3:0] r3_input;
	
	//sifra
	reg [15:0] password_reg;
	reg [15:0] password_next;
	
	reg [4:0] password_size_reg;
	reg [4:0] password_size_next;
	//test za sifru
	reg [15:0] password_test_reg;
	reg [15:0] password_test_next;
	
	reg [4:0] password_test_size_reg;
	reg [4:0] password_test_size_next;
	
	//broj pokusaja
	reg [2:0] test_count_reg;
	reg [2:0] test_count_next;
	
	//red_b
	wire red_b0, red_b1, red_b2;

	///////////////////////////////////////////////////////////
	//red
	red r0(clk, reset, but[0], red_b0);
	red r1(clk, reset, but[1], red_b1);
	red r2(clk, reset, but[2], red_b2);
	//bin_to_ss
	bin_to_ss dis0(
		.input_value(disp0_input),
		.output_value(HEX0)
	);
	
	bin_to_ss dis1(
		.input_value(disp1_input),
		.output_value(HEX1)
	);
	
	bin_to_ss dis2(
		.input_value(disp2_input),
		.output_value(HEX2)
	);
	
	bin_to_ss dis3(
		.input_value(disp3_input),
		.output_value(HEX3)
	);
	//reg9
	reg9 reg0(
		.clk(clk),
		.reset(reset),
		.load(r0_load),
		.dec(r0_dec),
		.output_value(r0_output),
		.input_value(r0_input)
	);
	
	reg9 reg1(
		.clk(clk),
		.reset(reset),
		.load(r1_load),
		.dec(r1_dec),
		.output_value(r1_output),
		.input_value(r1_input)
	);
	
	reg9 reg2(
		.clk(clk),
		.reset(reset),
		.load(r2_load),
		.dec(r2_dec),
		.output_value(r2_output),
		.input_value(r2_input)
	);
	
	reg9 reg3(
		.clk(clk),
		.reset(reset),
		.load(r3_load),
		.dec(r3_dec),
		.output_value(r3_output),
		.input_value(r3_input)
	);
	//////////////////////////////////////////////////////////
	//memory
	always @ (posedge reset, posedge clk) begin
		if(reset == 1) begin
			state_reg <= INICIJALIZACIJA;
			timer_reg <= 0;
			active_disp_reg <= 4'h0;
			
			//password
			password_reg <= 0;
			password_size_reg <= 0;
			
			//test password
			password_test_reg <= 0;
			password_test_size_reg <= 0;
			
			
			test_count_reg <= 0;
		end else begin
			state_reg <= state_next;
			timer_reg <= timer_next;
			active_disp_reg <= active_disp_next;
			
			//password
			password_reg <= password_next;
			password_size_reg <= password_size_next;
			
			//test password
			password_test_reg <= password_test_next;
			password_test_size_reg <= password_test_size_next;
			
			test_count_reg <= test_count_next;
		end
	end
	
	//next state logic
	always @ (*) begin
		timer_next = timer_reg;
		state_next = state_reg;
		active_disp_next = active_disp_reg;
		
		disp0_input = ISKLJUCEN_DISP;
		r0_dec = 0;
		r0_load = 0;
		r0_input = 0;
		
		disp1_input = ISKLJUCEN_DISP;
		r1_dec = 0;
		r1_load = 0;
		r1_input = 0;
		
		disp2_input = ISKLJUCEN_DISP;
		r2_dec = 0;
		r2_load = 0;
		r2_input = 0;
		
		disp3_input = ISKLJUCEN_DISP;
		r3_dec = 0;
		r3_load = 0;
		r3_input = 0;
		
		//password
		password_next = password_reg;
		password_size_next = password_size_reg;
		
		//test password
		password_test_next = password_test_reg;
		password_test_size_next = password_test_size_reg;
		
		//broj pokusaja
		test_count_next = test_count_reg;
		
		//led
		led[3:0] = 4'b0000;
		
		case(state_reg)
			INICIJALIZACIJA:
				begin
					r0_load = 1;
					r1_load = 1;
					r2_load = 1;
					r3_load = 1;
					
					//disp0
					r0_input = 4'h9;
					disp0_input = r0_output;
					active_disp_next[0] = 1;
					
					//disp1
					if(sw1 || sw2 || sw3) begin
						r1_input = 4'h9;
						active_disp_next[1] = 1;
						disp1_input = r1_output;
					end
					
					//disp2
					if(sw2 || sw3) begin
						r2_input = 4'h9;
						active_disp_next[2] = 1;
						disp2_input = r2_output;
					end
					
					//disp3
					if(sw3) begin
						r3_input = 4'h9;
						active_disp_next[3] = 1;
						disp3_input = r3_output;
					end
					
					//password
					if(red_b0 == 1) begin
						password_next[15:0] = {password_reg[13:0], BUTTON_0};
						password_size_next = (password_size_reg == 8)? 8 : password_size_reg + 1;
					end
					
					if(red_b1 == 1) begin
						password_next[15:0] = {password_reg[13:0], BUTTON_1};
						password_size_next = (password_size_reg == 8)? 8 : password_size_reg + 1;
					end
					
					if(red_b2 == 1) begin
						password_next[15:0] = {password_reg[13:0], BUTTON_2};
						password_size_next = (password_size_reg == 8)? 8 : password_size_reg + 1;
					end
					
					//next stete
					if(start == 1) begin
						timer_next = 0;
						state_next = ODBROJAVANJE;
					end
				end
			
			ODBROJAVANJE:
				begin

					disp0_input = (active_disp_reg[0] == 1)? r0_output : ISKLJUCEN_DISP;
					disp1_input = (active_disp_reg[1] == 1)? r1_output : ISKLJUCEN_DISP;
					disp2_input = (active_disp_reg[2] == 1)? r2_output : ISKLJUCEN_DISP;
					disp3_input = (active_disp_reg[3] == 1)? r3_output : ISKLJUCEN_DISP;
					
					if(timer_reg == SEKUNDA) begin
						r0_dec = 1;
						timer_next = 0;
						
						if(r0_output == 0) begin
							r1_dec = 1;		
							
							r0_load = 1;
							r0_input = 4'h9;
						end
						
						if(r0_output == 0 && r1_output == 0) begin
							r2_dec = 1;
							
							r1_load = 1;
							r1_input = 4'h9;
						end
						
						if(r0_output == 0 && r1_output == 0 && r2_output == 0) begin
							r3_dec = 1;
							
							r2_load = 1;
							r2_input = 4'h9;
						end
						
					end else
						timer_next = timer_reg + 1;
					
					//test password
					if(red_b0 == 1) begin
						password_test_next[15:0] = {password_test_reg[13:0], BUTTON_0};
						password_test_size_next = (password_test_size_reg == 8)? 8 : password_test_size_reg + 1;
					end
					
					if(red_b1 == 1) begin
						password_test_next[15:0] = {password_test_reg[13:0], BUTTON_1};
						password_test_size_next = (password_test_size_reg == 8)? 8 : password_test_size_reg + 1;
					end
					
					if(red_b2 == 1) begin
						password_test_next[15:0] = {password_test_reg[13:0], BUTTON_2};
						password_test_size_next = (password_test_size_reg == 8)? 8 : password_test_size_reg + 1;
					end
					
					//proveri da li se poklapa
					if(password_size_reg == password_test_size_reg) begin
						if(password_reg == password_test_reg) begin
							state_next = ZAMRZNUTO;
						end else begin
							test_count_next = test_count_reg + 1;
							
							password_test_size_next = 0;
							password_test_next = 0;
						end
					
					end
					
					//leds
					if(test_count_reg > 3'b000) begin
						led[0] = 1;
					end
					
					if(test_count_reg > 3'b001) begin
						led[1] = 1;
					end	
					
					if(test_count_reg > 3'b010) begin
						led[2] = 1;
					end	
					if(test_count_reg > 3'b011) begin
						led[3] = 1;
					end

					//next state
					if((r0_output == 0 && r1_output == 0 && r2_output == 0 && r3_output == 0) || test_count_reg == 3'b100) begin
						state_next = TREPTANJE;
						timer_next = 0;
					end
				end
			TREPTANJE:
				begin
					timer_next = (timer_reg == SEKUNDA)? 0: timer_reg + 1;
					
					if(test_count_reg == 3'b100) begin
						led[3:0] = 4'b1111;
					end
					
					
					if(timer_reg < POLA_SEKUNDE) begin
						disp0_input = ISKLJUCEN_DISP;
						disp1_input = ISKLJUCEN_DISP;
						disp2_input = ISKLJUCEN_DISP;
						disp3_input = ISKLJUCEN_DISP;
					end else begin
						disp0_input = (active_disp_reg[0])? TREPTI_DISP: ISKLJUCEN_DISP;	
						disp1_input = (active_disp_reg[1])? TREPTI_DISP: ISKLJUCEN_DISP;
						disp2_input = (active_disp_reg[2])? TREPTI_DISP: ISKLJUCEN_DISP;
						disp3_input = (active_disp_reg[3])? TREPTI_DISP: ISKLJUCEN_DISP;
					end
				end
			ZAMRZNUTO:
				begin
					disp0_input = (active_disp_reg[0] == 1)? r0_output : ISKLJUCEN_DISP;
					disp1_input = (active_disp_reg[1] == 1)? r1_output : ISKLJUCEN_DISP;
					disp2_input = (active_disp_reg[2] == 1)? r2_output : ISKLJUCEN_DISP;
					disp3_input = (active_disp_reg[3] == 1)? r3_output : ISKLJUCEN_DISP;
				
					//leds
					if(test_count_reg > 3'b000) begin
						led[0] = 1;
					end
					
					if(test_count_reg > 3'b001) begin
						led[1] = 1;
					end	
					
					if(test_count_reg > 3'b010) begin
						led[2] = 1;
					end	
					if(test_count_reg > 3'b011) begin
						led[3] = 1;
					end
				
				end
		endcase
	end
	
endmodule
























