module reg9(
	input wire clk,
	input wire reset,
	input wire load,
	input wire [3:0] input_value,
	input wire dec,
	output wire [3:0] output_value
);

	reg [3:0] value_reg, value_next;
	
	//memory
	always @ (posedge clk, posedge reset) begin
		if(reset)
			value_reg <= 0;
		else 
			value_reg <= value_next;
	end
	
	//next state
	always @ (*) begin
		value_next = value_reg;
	
		if(load == 1)
			value_next = input_value;
		else if(dec == 1 && value_reg != 4'h0)
			value_next = value_reg - 1;
	end
	
	assign output_value = value_reg;
	
endmodule
























