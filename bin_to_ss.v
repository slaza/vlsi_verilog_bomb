/*
	binary[3:0] to ss display[6:0]
*/

module bin_to_ss(
	input wire [3:0] input_value,
	output reg [6:0] output_value
);	
	
	always @ (*) begin
		case(input_value)
			4'h0:
				begin
					output_value = 7'b100_0000;
				end
			4'h1:
				begin
					output_value = 7'b111_1001;
				end
			4'h2:
				begin
					output_value = 7'b010_0100;
				end
			4'h3:
				begin
					output_value = 7'b011_0000;
				end
			4'h4:
				begin
					output_value = 7'b001_1001;
				end
			4'h5:
				begin
					output_value = 7'b001_0010;
				end
			4'h6:
				begin
					output_value = 7'b000_0010;
				end
			4'h7:
				begin
					output_value = 7'b111_1000;
				end
			4'h8:
				begin
					output_value = 7'b000_0000;
				end
			4'h9:
				begin
					output_value = 7'b001_0000;
				end
			4'hA:
				begin
					output_value = 7'b011_1111;
				end
			default:
				begin
					output_value = 7'b111_1111;
				end
		
		endcase
	end
	
endmodule
