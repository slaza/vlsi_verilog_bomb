/*
	rising edge detector
*/

module red(
	input wire clk, 
	input wire reset,
	input wire input_value,
	output wire output_value
);
	
	
	reg ff1_reg, ff1_next;
	reg ff2_reg, ff2_next;
	
	//memory
	always @ (posedge reset, posedge clk) begin
		if(reset) begin
			ff1_reg <= 0;
			ff2_reg <= 0;
		end else begin
			ff1_reg <= ff1_next;
			ff2_reg <= ff2_next;
		end
	end
	
	//next state
	always @ (*) begin
		ff1_next = input_value;
		ff2_next = ff1_reg;
	end
	
	assign output_value = (ff1_reg && !ff2_reg);
	
endmodule
