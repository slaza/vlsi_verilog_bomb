module test();
	reg clk, reset, start;
	reg sw1, sw2, sw3;
	
	reg [2:0] but;
	
	
	wire [6:0] HEX0;
	wire [6:0] HEX1;
	wire [6:0] HEX2;
	wire [6:0] HEX3;
	
	wire [3:0] led;

	main m1(
		.reset(reset),
		.start(start),
		.clk(clk),
		
		.HEX0(HEX0),
		.HEX1(HEX1),
		.HEX2(HEX2),
		.HEX3(HEX3),
		
		.sw1(sw1),
		.sw2(sw2),
		.sw3(sw3),
		
		.but(but),
		.led(led)
	);
	
	
	defparam m1.SEKUNDA = 3;
	defparam m1.POLA_SEKUNDE = 1;
	
	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0, test);
		
		clk = 0;
		start = 0;
		reset = 0;
		sw1 = 0;
		sw2 = 0;
		sw3 = 0;
		
		but = 3'b000;
	end
	
	always begin
		#1 clk = ~clk;
	end
	
	initial begin
		//9
		reset = 1;
		#2 reset = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 start = 1;
		#2 start = 0;
		
		//dobra kombinacija
		#14 but[0] = 1;
		#2 but[0] = 0;
		
		#4 but[1] = 1;
		#2 but[1] = 0;

		//99----------
		#30 reset = 1;
		#2 reset = 0;
		//////////////
		#4 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		//////////////
		#8 sw1 = 1;
		#4 start = 1;
		#2 start = 0;
		#2 sw1 = 0;
		
		//pogresna kombinacija
		#8 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		//dobra kombinacija
		#8 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		//999---------
		#30 reset = 1;
		#2 reset = 0;
		//////////////
		#10 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		//////////////
		#2 sw2 = 1;
		#4 start = 1;
		#2 start = 0;
		#2 sw2 = 0;
		
		//pogresna kombinacija 1-4
		#6 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		//pogresna kombinacija -2
		#10 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		//pogresna kombinacija -3
		#10 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		//pogresna kombinacija -4
		#10 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		
		//9999
		#30 reset = 1;
		#2 reset = 0;
		
		//////////////
		#10 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#4 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		//////////////
		#2 sw3 = 1;
		#4 start = 1;
		#2 start = 0;
		#2 sw3 = 0;
		
		//pogresna kombinacija -1
		#30 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		
		//pogresna kombinacija -2
		#30 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;

		//pogresna kombinacija -3
		#30 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;

		//pogresna kombinacija -4
		#30 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[0] = 1;
		#2 but[0] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[2] = 1;
		#2 but[2] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#2 but[1] = 1;
		#2 but[1] = 0;
		
		#20 $finish;
	end
endmodule






